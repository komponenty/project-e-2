/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.project.logika.internal;

import java.awt.Dimension;
import java.awt.Image;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import org.apache.felix.scr.annotations.*;
import org.osgi.framework.BundleContext;
import pk.project.contracts.ILogika;
import pk.project.plansza.IPlansza;
import pk.project.plansza.IPole;

/**
 *
 * @author Adam_2
 */
@Service
@Component
public class Logika implements ILogika {

    @Reference
    private IPlansza plansza;
    private BundleContext context;

    private ArrayList<String> ulozeniePolZwyciestwo;
    private ArrayList<Character> nazwyPrzyciskow1;
    private ArrayList<Character> nazwyPrzyciskow2;
    private int licznikKrokow1;
    private int licznikKrokow2;
    private long start;
    private long stop;
    private int ktoraListeKrokowZwrocic;
    private int liczbaPunktow;

    public Logika() {
        this.nazwyPrzyciskow1 = new ArrayList<Character>();
        this.nazwyPrzyciskow2 = new ArrayList<Character>();
        nazwyPrzyciskow1.add('a');
        nazwyPrzyciskow1.add('w');
        nazwyPrzyciskow1.add('d');
        nazwyPrzyciskow1.add('s');
        nazwyPrzyciskow2.add('j');
        nazwyPrzyciskow2.add('i');
        nazwyPrzyciskow2.add('l');
        nazwyPrzyciskow2.add('k');

    }

    @Activate
    public void activate(BundleContext context) {
        this.context = context;
        System.out.println("Komponent logika zaladowany.");

    }

    protected void bindPlansza(IPlansza plansza) {
        this.plansza = plansza;
    }

    private ArrayList<IPole> getListaPrzyciskow(ArrayList<IPole> listaPrzyciskow, int poziomInt) {

        for (int i = 0; i < poziomInt; i++) {
            for (int j = 0; j < poziomInt; j++) {
                listaPrzyciskow.add(plansza.getPole(j, i));
            }
        }
        return listaPrzyciskow;
    }

    public void GenerujPojedyncza(JFrame rozgrywka, JPanel panel, JFrame podsumowanie, int poziomInt, ArrayList<Image> listaPocietychObrazkow) {
        this.licznikKrokow1 = 0;
        this.liczbaPunktow =0;
        ArrayList<IPole> listaPrzyciskow1 = new ArrayList<IPole>();
        this.generujPlansze(panel, poziomInt, listaPocietychObrazkow, listaPrzyciskow1);
        //Dodawanie zdarzeń klikania myszką
        plansza.DodajActionPol(this.listenerKliknieciaMyszka(podsumowanie, listaPrzyciskow1, poziomInt, this.SprawdzCzyJestGrafika(listaPocietychObrazkow)));
        rozgrywka.addKeyListener(this.listenerKliknieciaPrzyciskow(podsumowanie, null, listaPrzyciskow1, null, poziomInt, this.SprawdzCzyJestGrafika(listaPocietychObrazkow)));
        rozgrywka.setFocusable(true);
        this.start = System.currentTimeMillis();
    }

    public void GenerujPojedynek(JFrame rozgrywka, JPanel panel1, JPanel panel2, JFrame podsumowanie1, JFrame podsumowanie2, int poziomInt, ArrayList<Image> listaPocietychObrazkow) {
        this.ktoraListeKrokowZwrocic = 0;
        this.liczbaPunktow = 0;
        this.licznikKrokow1 = 0;
        this.licznikKrokow2 = 0;
        ArrayList<IPole> listaPrzyciskow1 = new ArrayList<IPole>();
        this.generujPlansze(panel1, poziomInt, listaPocietychObrazkow, listaPrzyciskow1);
        //plansza.DodajActionPol(this.listenerKliknieciaMyszka2(rozgrywka));

        ArrayList<IPole> listaPrzyciskow2 = new ArrayList<IPole>();
        this.generujPlansze(panel2, poziomInt, listaPocietychObrazkow, listaPrzyciskow2);
        // plansza.DodajActionPol(this.listenerKliknieciaMyszka2(rozgrywka));
        panel2.setLocation(panel1.getSize().width + 100, panel1.getLocation().y);
        rozgrywka.addKeyListener(this.listenerKliknieciaPrzyciskow(podsumowanie1, podsumowanie2, listaPrzyciskow1, listaPrzyciskow2, poziomInt, this.SprawdzCzyJestGrafika(listaPocietychObrazkow)));
        rozgrywka.setFocusable(true);
        this.start = System.currentTimeMillis();
    }

    private void generujPlansze(JPanel jpanel, int poziomInt, ArrayList<Image> listaPocietychObrazkow, ArrayList<IPole> listaPrzyciskow) {

        plansza.GenerujPlansze2(poziomInt, 100, 0, 0, null, null, jpanel);
        listaPrzyciskow = this.getListaPrzyciskow(listaPrzyciskow, poziomInt);
        this.wypelnijPrzyciskiNapisami(poziomInt, listaPrzyciskow);
        boolean czyObrazek = false;
        if (this.SprawdzCzyJestGrafika(listaPocietychObrazkow)) {
            this.wypelnijPrzyciskiObrazkami(poziomInt, listaPrzyciskow, listaPocietychObrazkow);
            czyObrazek = true;
        }
        //plansza.DodajActionPol(this.listenerKliknieciaMyszka(listaPrzyciskow, poziomInt, czyObrazek));
        jpanel.setPreferredSize(new Dimension(poziomInt * 100 + 5, poziomInt * 100 + 27));
        this.pomieszajPola(poziomInt, listaPrzyciskow, czyObrazek);
        this.ustawWolnePole(poziomInt, listaPrzyciskow);
        this.ustawMozliweRuchy(listaPrzyciskow, poziomInt);

    }

    private boolean SprawdzCzyJestGrafika(ArrayList<Image> listaPocietychObrazkow) {
        if (listaPocietychObrazkow != null) {
            return true;
        } else {
            return false;
        }
    }

    private void wypelnijPrzyciskiNapisami(int poziomInt, ArrayList<IPole> listaPrzyciskow) {
        this.ulozeniePolZwyciestwo = new ArrayList<String>();
        for (int i = 0; i < poziomInt * poziomInt; i++) {
            listaPrzyciskow.get(i).getButton().setText("b" + (i + 1));
            this.ulozeniePolZwyciestwo.add(listaPrzyciskow.get(i).getButton().getText());

        }
    }

    private void wypelnijPrzyciskiObrazkami(int poziomInt, ArrayList<IPole> listaPrzyciskow, ArrayList<Image> listaObrazkow) {

        for (int i = 0; i < poziomInt * poziomInt; i++) {
            listaPrzyciskow.get(i).getButton().setIcon(new ImageIcon(listaObrazkow.get(i)));
            Image a = listaObrazkow.get(i);
            BufferedImage b = (BufferedImage) a;
            listaPrzyciskow.get(i).UstawObrazek(b);
        }

    }

    private void pomieszajPola(int poziomInt, ArrayList<IPole> listaPrzyciskow, boolean czyObrazek) {
        Random rand = new Random();
        int pomocInt = 0;
        int pomocInt2 = 0;

        if (czyObrazek == true) {

            for (int i = 0; i < poziomInt * poziomInt; i++) {
                pomocInt = rand.nextInt(listaPrzyciskow.size());
                pomocInt2 = rand.nextInt(listaPrzyciskow.size());
                this.ruchZObrazkiem(listaPrzyciskow.get(pomocInt).getButton(), listaPrzyciskow.get(pomocInt2).getButton());

                //prawdopodobnie trzeba zmieniać ulozeniePol1
            }
        } else if (czyObrazek == false) {
            for (int i = 0; i < poziomInt * poziomInt; i++) {
                pomocInt = rand.nextInt(listaPrzyciskow.size());
                pomocInt2 = rand.nextInt(listaPrzyciskow.size());
                this.ruchBezObrazka(listaPrzyciskow.get(pomocInt).getButton(), listaPrzyciskow.get(pomocInt2).getButton());

            }
        }

    }

    private void ustawWolnePole(int poziomInt, ArrayList<IPole> lista_przyciskow1) {
        for (int i = 0; i < poziomInt * poziomInt; i++) {

            if (lista_przyciskow1.get(i).getButton().getText().equals(this.ulozeniePolZwyciestwo.get(poziomInt * poziomInt - 1))) {
                lista_przyciskow1.get(i).getButton().setVisible(false);
            }
        }
    }

    private void ruchZObrazkiem(JButton button1, JButton button2) {
        Icon iconPomoc;
        iconPomoc = button1.getIcon();
        button1.setIcon(button2.getIcon());
        button2.setIcon(iconPomoc);
        this.ruchBezObrazka(button1, button2);

    }

    private void ruchBezObrazka(JButton button1, JButton button2) {
        String pomocString;
        pomocString = button1.getText();
        button1.setText(button2.getText());
        button2.setText(pomocString);
        if (button1.getText().equals(this.ulozeniePolZwyciestwo.get(this.ulozeniePolZwyciestwo.size() - 1))) {
            button1.setVisible(false);
            button2.setVisible(true);
        } else if (button2.getText().equals(this.ulozeniePolZwyciestwo.get(this.ulozeniePolZwyciestwo.size() - 1))) {
            button2.setVisible(false);
            button1.setVisible(true);
        }

    }

    private int wyszukajIndeksWolnegoPola(ArrayList<IPole> listaPrzyciskow, int poziomInt) {
        int indeks = -1;
        for (int i = 0; i < (poziomInt * poziomInt); i++) {
            if (listaPrzyciskow.get(i).getButton().getText().equals(this.ulozeniePolZwyciestwo.get(poziomInt * poziomInt - 1))) {
                indeks = i;
                break;

            }
        }
        return indeks;
    }

    private IPole zwrocPoleONazwie(ArrayList<IPole> listaPrzyciskow, String text) {
        IPole pole = null;
        for (int i = 0; i < listaPrzyciskow.size(); i++) {
            if (listaPrzyciskow.get(i).getButton().getText().equals(text)) {
                pole = listaPrzyciskow.get(i);
                break;
            }

        }
        return pole;

    }

    private void ustawMozliweRuchy(ArrayList<IPole> listaPrzyciskow, int poziomInt) {
        int pozycja = this.wyszukajIndeksWolnegoPola(listaPrzyciskow, poziomInt);
        if (pozycja != -1) {
            //dodaje wszystkie mozliwe ruchy
            ArrayList<Integer> listaMozliwychRuchow = new ArrayList<Integer>();
            listaMozliwychRuchow.add(pozycja - poziomInt);
            listaMozliwychRuchow.add(pozycja - 1);
            listaMozliwychRuchow.add(pozycja + 1);
            listaMozliwychRuchow.add(pozycja + poziomInt);

            //werifikuje ktore ruchy mozna wykonac i to pozycja pola
            for (int i = 0; i < listaMozliwychRuchow.size(); i++) {
                if (listaMozliwychRuchow.get(i) + 1 > poziomInt * poziomInt) {
                    listaMozliwychRuchow.remove(listaMozliwychRuchow.get(i));
                    i -= 1;

                } else if (listaMozliwychRuchow.get(i) + 1 < 1) {
                    listaMozliwychRuchow.remove(listaMozliwychRuchow.get(i));
                    i -= 1;

                }

            }

            for (int i = 0; i < listaMozliwychRuchow.size(); i++) {
                //wyrzuca ruch -1 z pierwszego rzadka 
                if ((pozycja + 1) % poziomInt == 1 && listaMozliwychRuchow.contains(pozycja - 1)) {
                    listaMozliwychRuchow.remove(listaMozliwychRuchow.indexOf(pozycja - 1));
                    i -= 1;
                    //wyrzuca ruch +1 z ostatniego rzadka
                } else if ((pozycja + 1) % poziomInt == 0 && listaMozliwychRuchow.contains(pozycja + 1)) {

                    listaMozliwychRuchow.remove(listaMozliwychRuchow.indexOf(pozycja + 1));
                    i -= 1;
                }
            }

            //ustawia pole ,po ktorym bedzie mozna sprawdzic czy dany ruch jest mozliwy do wykonania
            for (int i = 0; i < listaPrzyciskow.size(); i++) {

                listaPrzyciskow.get(i).UstawCzyWolne(false);
            }

            //ustawia pola, ktore mozna przesunac na wolne miejsce
            for (int i = 0; i < listaMozliwychRuchow.size(); i++) {

                listaPrzyciskow.get(listaMozliwychRuchow.get(i)).UstawCzyWolne(true);

            }

        }

    }

    private boolean sprawdzCzyUlozono(ArrayList<IPole> listaPrzyciskow) {
        int licznikZgodnosci = 0;
        for (int i = 0; i < listaPrzyciskow.size(); i++) {
            if (this.ulozeniePolZwyciestwo.get(i).equals(listaPrzyciskow.get(i).getButton().getText())) {
                licznikZgodnosci += 1;
            } else {
                break;
            }

        }
        if (licznikZgodnosci == this.ulozeniePolZwyciestwo.size()) {
            return true;
        } else {
            return false;
        }

    }

    private ActionListener listenerKliknieciaMyszka(JFrame frame1, ArrayList<IPole> listaPrzyciskow1, int poziomInt1, boolean czyGrafka1) {

        final JFrame frame = frame1;
        final ArrayList<IPole> listaPrzyciskow = listaPrzyciskow1;
        final int poziomInt = poziomInt1;
        final boolean czyGrafika = czyGrafka1;

        ActionListener buttAct = new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                JButton klikniety = (JButton) e.getSource();
                IPole wolnePole = listaPrzyciskow.get(wyszukajIndeksWolnegoPola(listaPrzyciskow, poziomInt));
                IPole kliknietyPole = zwrocPoleONazwie(listaPrzyciskow, klikniety.getText());
                //sprawdza czy można przesunąć wolne pole na klikniety
                if (kliknietyPole != null && kliknietyPole.CzyWolne()) {
                    dodajKrok1();
                    if (czyGrafika) {
                        ruchZObrazkiem(kliknietyPole.getButton(), wolnePole.getButton());
                    } else {
                        ruchBezObrazka(kliknietyPole.getButton(), wolnePole.getButton());
                    }

                    //Ustawia nowe dozwolone ruchy
                    ustawMozliweRuchy(listaPrzyciskow, poziomInt);
                }

                //Sprawdza czy wygrana zrobić metodę zwaracjacą boolean
                if (sprawdzCzyUlozono(listaPrzyciskow)) {
                    stop = System.currentTimeMillis();
                    ktoraListeKrokowZwrocic = 1;
                    UstawLiczbePunktow(poziomInt, licznikKrokow1);
                    frame.setVisible(true);
                }

            }
        };

        return buttAct;
    }

    private IPole zwrocPoleOWybranymKeyCode(char keyCode, ArrayList<IPole> listaPrzyciskow, ArrayList<Character> listaNazwPrzyciskow, int poziomInt) {
        int wolnePole = this.wyszukajIndeksWolnegoPola(listaPrzyciskow, poziomInt);
        IPole klikniety = null;
        // góra
        if (keyCode == listaNazwPrzyciskow.get(1)) {
            if (wolnePole + poziomInt < poziomInt * poziomInt) {
                klikniety = listaPrzyciskow.get(wolnePole + poziomInt);
            }

            //lewo
        } else if (keyCode == listaNazwPrzyciskow.get(0)) {
            if (wolnePole + 1 < poziomInt * poziomInt) {
                klikniety = listaPrzyciskow.get(wolnePole + 1);
            }

            //prawo
        } else if (keyCode == listaNazwPrzyciskow.get(2)) {
            if (wolnePole - 1 >= 0) {
                klikniety = listaPrzyciskow.get(wolnePole - 1);
            }

            //dol
        } else if (keyCode == listaNazwPrzyciskow.get(3)) {
            if (wolnePole - poziomInt >= 0) {
                klikniety = listaPrzyciskow.get(wolnePole - poziomInt);
            }

        }
        return klikniety;
    }

    private KeyListener listenerKliknieciaPrzyciskow(JFrame podsumowanie1, JFrame podsumowanie21, ArrayList<IPole> listaPrzyciskow1, ArrayList<IPole> listaPrzyciskow21, int poziomInt1, boolean czyGrafka1) {

        final JFrame podsumowanie = podsumowanie1;
        final JFrame podsumowanie2 = podsumowanie21;
        final ArrayList<IPole> listaPrzyciskow = listaPrzyciskow1;
        final ArrayList<IPole> listaPrzyciskow2 = listaPrzyciskow21;
        final int poziomInt = poziomInt1;
        final boolean czyGrafika = czyGrafka1;
        final ArrayList<Character> nazwyPrzyciskow1 = this.nazwyPrzyciskow1;
        final ArrayList<Character> nazwyPrzyciskow2 = this.nazwyPrzyciskow2;

        KeyListener keyPressed = new KeyListener() {
            public void keyPressed(KeyEvent e) {
                char keyCode = e.getKeyChar();
                //sprawdza listy w ktoryej znajduje się wybrany key jeśli w pierwszej 1 jeśli w drugiej 2 jeśli w żadneh -1
                int planszaInt = sprawdzKtoraPlanszaWykonalaRuch(keyCode, nazwyPrzyciskow1, nazwyPrzyciskow2);
                if (planszaInt == 1 && listaPrzyciskow != null) {
                    wykonajOperacjeDlaKeyListenera(keyCode, podsumowanie, listaPrzyciskow, nazwyPrzyciskow1, poziomInt, czyGrafika, 1);
                } else if (planszaInt == 2 && listaPrzyciskow2 != null) {

                    wykonajOperacjeDlaKeyListenera(keyCode, podsumowanie2, listaPrzyciskow2, nazwyPrzyciskow2, poziomInt, czyGrafika, 2);
                }

            }

            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyReleased(KeyEvent e) {

            }

        };
        return keyPressed;
    }

    private int sprawdzKtoraPlanszaWykonalaRuch(char keyCode, ArrayList<Character> nazwyPrzyciskow1, ArrayList<Character> nazwyPrzyciskow2) {

        if (nazwyPrzyciskow1.contains(keyCode) && nazwyPrzyciskow1 != null) {
            return 1;
        } else if (nazwyPrzyciskow2.contains(keyCode) && nazwyPrzyciskow2 != null) {
            return 2;
        }

        return -1;
    }

    private void wykonajOperacjeDlaKeyListenera(char keyCode, JFrame podsumowanie, ArrayList<IPole> listaPrzyciskow, ArrayList<Character> nazwyPrzyciskow, int poziomInt, boolean czyGrafika, int ktoryLicznikKrokow) {
        IPole kliknietyPole = zwrocPoleOWybranymKeyCode(keyCode, listaPrzyciskow, nazwyPrzyciskow, poziomInt);
        IPole wolnePole = listaPrzyciskow.get(wyszukajIndeksWolnegoPola(listaPrzyciskow, poziomInt));

        //sprawdza czy można przesunąć wolne pole na klikniety
        if (kliknietyPole != null && kliknietyPole.CzyWolne()) {
            if (ktoryLicznikKrokow == 1) {
                dodajKrok1();
            } else if (ktoryLicznikKrokow == 2) {
                dodajKrok2();
            }

            if (czyGrafika) {
                ruchZObrazkiem(kliknietyPole.getButton(), wolnePole.getButton());
            } else {
                ruchBezObrazka(kliknietyPole.getButton(), wolnePole.getButton());
            }

            //Ustawia nowe dozwolone ruchy
            ustawMozliweRuchy(listaPrzyciskow, poziomInt);
        }

        //Sprawdza czy wygrana zrobić metodę zwaracjacą boolean
        if (sprawdzCzyUlozono(listaPrzyciskow)) {
            //  if (licznikKrokow2 > 2 || licznikKrokow1 > 2) {

            stop = System.currentTimeMillis();
            ktoraListeKrokowZwrocic = ktoryLicznikKrokow;
            UstawLiczbePunktow(poziomInt, licznikKrokow1);
            podsumowanie.setVisible(true);
        }
    }

    public String ZwrocCzas() {
        return String.valueOf((stop - start) / 1000 / 60 + " min " + ((stop - start) / 1000) % 60 + " s");
    }
// poprawić

    public int ZwrocLiczbeKrokow() {
        if (ktoraListeKrokowZwrocic == 1) {
            return this.licznikKrokow1;
        } else if (ktoraListeKrokowZwrocic == 2) {
            return this.licznikKrokow2;
        }

        return -1;
    }

    public  int  ZwrocLiczbePunktow() {
        return this.liczbaPunktow;
        

    }

    private void dodajKrok2() {
        this.licznikKrokow2 += 1;
    }

    private void dodajKrok1() {
        this.licznikKrokow1 += 1;
    }
    private void UstawLiczbePunktow(int poziom,int liczbaKrokow){
        this.liczbaPunktow = (poziom*poziom*100) - liczbaKrokow - (int)(stop - start) / 10000*5 ;
    }
}
