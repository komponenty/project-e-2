/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.project.fileOperations.internal;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import pk.project.contracts.IFileOperations;
import org.osgi.framework.BundleContext;
import org.apache.felix.scr.annotations.*;

/**
 *
 * @author Adam_2
 */
@Service
@Component
public class FileOperations implements IFileOperations {

    String pathToImages;
    private BundleContext context;

    public FileOperations() {
        try {
            this.pathToImages = new File(".").getCanonicalPath() + "\\Images\\";
        } catch (IOException ex) {
            Logger.getLogger(FileOperations.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Activate
    public void activate(BundleContext context) {
        this.context = context;

        System.out.println("Komponent fileOpertaions zaladowany.");
    }

    @Override
    public boolean CheckIfFileExists(String fileName) {

        File file = new File(this.pathToImages + fileName);
        if (file.exists()) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void AddImage(String pathToFile) {

        InputStream inStream = null;
        OutputStream outStream = null;

        try {

            File copyFile = new File(pathToFile);
            File pasteFile = new File(this.pathToImages + copyFile.getName());
            inStream = new FileInputStream(copyFile);
            outStream = new FileOutputStream(pasteFile);
            byte[] buffer = new byte[1024];
            int length;

            while ((length = inStream.read(buffer)) > 0) {

                outStream.write(buffer, 0, length);
            }
            inStream.close();
            outStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public List<String> GetImagesNamesOnDirectory() {
        try {
            File dir = new File(this.pathToImages);
            String[] tableNames = dir.list();
            List<String> ImagesNamesOnDirectory = Arrays.asList(tableNames);
            return ImagesNamesOnDirectory;
        } catch (Exception e) {
            return new ArrayList<>();
        }
    }

    @Override
    public String GetPathToImages(String fileName) {
        return this.pathToImages + fileName;
    }

}
