/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.project.contracts;

import java.util.List;

/**
 *
 * @author Adam_2
 */
public interface IFileOperations {

    public String GetPathToImages(String fileName);

    public List<String> GetImagesNamesOnDirectory();

    public void AddImage(String pathToFile);

    public boolean CheckIfFileExists(String fileName);
}
