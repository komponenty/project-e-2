/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.project.contracts;

import java.awt.Image;
import java.util.List;

/**
 *
 * @author Adam_2
 */
public interface IGraphic {

    public void SetListOfPartsImage(String pathToImage, int numberParts);

    public List<Image> ScaleListImage(List<Image> ListImage, int maxWidth, int maxHeight);

    public Image ScaleImage(Image image, int maxWidth, int maxHeight);

    public List<Image> getListOfPartsImage();

}
