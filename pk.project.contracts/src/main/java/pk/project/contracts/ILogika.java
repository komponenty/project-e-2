/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.project.contracts;

import java.awt.Image;
import java.util.ArrayList;
import javax.swing.JFrame;
import javax.swing.JPanel;

/**
 *
 * @author Adam_2
 */
public interface ILogika {

    public void GenerujPojedyncza(JFrame rozgrywka, JPanel panel, JFrame podsumowanie, int poziomInt, ArrayList<Image> listaPocietychObrazkow);

    public void GenerujPojedynek(JFrame rozgrywka, JPanel panel1, JPanel panel2, JFrame podsumowanie1, JFrame podsumowanie2, int poziomInt, ArrayList<Image> listaPocietychObrazkow);

    public String ZwrocCzas();

    public int ZwrocLiczbeKrokow();

    public int ZwrocLiczbePunktow();

    
}
