/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.project.gui.internal;

/**
 *
 * @author Adam_2
 */
public class DaneZGry {

    private String nickUżytkownika1;
    private String poziom;
    private String rodzajGrafiki;
    private String nazwaObrazka;
    private int liczbaKrokow;
    private String czas;
    private int liczbaPunktow;

    public DaneZGry(String nickUżytkownika1, String poziom, String rodzajGrafiki, String nazwaObrazka) {
        this.nickUżytkownika1 = nickUżytkownika1;
        this.liczbaKrokow = 52;
        this.poziom = poziom;
        this.rodzajGrafiki = rodzajGrafiki;
        this.nazwaObrazka = nazwaObrazka;

    }

    public String GetNickUżytkownika1() {
        return this.nickUżytkownika1;
    }

    public String GetPoziom() {
        return this.poziom;
    }

    public String GetRodzajGrafiki() {
        return this.rodzajGrafiki;
    }

    public String GetNazwaObrazka() {
        return this.nazwaObrazka;
    }

    public int GetLiczbaKrokow() {
        return this.liczbaKrokow;
    }

    public void SetLiczbaKrokow(int liczbaKrokow) {
        this.liczbaKrokow = liczbaKrokow;
    }

    public String GetCzas() {
        return this.czas;
    }

    public void SetCzas(String czas) {
        this.czas = czas;
    }
    public int GetLiczbaPunktow() {
        return this.liczbaPunktow;
    }

    public void SetLiczbPunktow(int liczbaPunktow) {
        this.liczbaPunktow = liczbaPunktow;
    }
}
