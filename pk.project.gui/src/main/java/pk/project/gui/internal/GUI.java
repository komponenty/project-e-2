/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.project.gui.internal;

import org.osgi.framework.BundleContext;
import org.apache.felix.scr.annotations.*;
import pk.project.contracts.*;
import pk.project.database.IDatabase;
/**
 *
 * @author Adam_2
 */

@Component(immediate = true)
public class GUI  {

    @Reference
    private IGraphic g;
    @Reference
    private IFileOperations fo;
    @Reference
    private ILogika logika;
    @Reference
    private IDatabase database;
    @Reference
    private IMuzyka muzyka;
    private Menu menu;
    
    java.util.List<String> list;

    public GUI() {

    }

    private BundleContext context;

    @Activate
    public void activate(BundleContext context) {
        this.context = context;

        System.out.println("Komponent gui zaladowany.");
        Start();
    }

    protected void bindG(IGraphic g) {
        this.g = g;
    }

    protected void bindFo(IFileOperations fo) {
        this.fo = fo;
    }
     protected void bindLogika(ILogika logika) {
        this.logika = logika;
    }
    protected void binddatabase(IDatabase database) {
        this.database = database;
    }
     protected void bindMuzyka(IMuzyka muzyka) {
        this.muzyka = muzyka;
    }
    

    @Deactivate
    public void deactivate() {
        this.context = null;
    }

    
    public Menu GetMenu() {
        return this.menu;
    }

    private void Start() {

        this.menu = new Menu(g, fo,logika,database,muzyka);
        this.menu.setVisible(true);
        

    }

}
