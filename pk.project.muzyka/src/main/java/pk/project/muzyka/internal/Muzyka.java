/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.project.muzyka.internal;

import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import org.apache.felix.scr.annotations.Activate;
import org.apache.felix.scr.annotations.Component;
import org.apache.felix.scr.annotations.Service;
import org.osgi.framework.BundleContext;
import pk.project.contracts.IMuzyka;

/**
 *
 * @author Adam_2
 */
@Service
@Component
public class Muzyka implements IMuzyka {

    private Clip c;
    private AudioInputStream ais;
    private boolean wyciszony;
     private BundleContext context;

    public Muzyka() {
        this.wyciszony = false;
    }
 @Activate
    public void activate(BundleContext context) {
        this.context = context;
        System.out.println("Komponent muzyka zaladowany.");

    }
    public void Play(String filePath) {

        if (!wyciszony && this.akcteptacja(filePath)) {

            try {
                ais = AudioSystem.getAudioInputStream(new File(filePath).getAbsoluteFile());
                c = AudioSystem.getClip();
                c.open(ais);
                c.start();
            } catch (UnsupportedAudioFileException ex) {
                Logger.getLogger(Muzyka.class.getName()).log(Level.SEVERE, null, ex);
            } catch (IOException ex) {
                Logger.getLogger(Muzyka.class.getName()).log(Level.SEVERE, null, ex);
            } catch (LineUnavailableException ex) {
                Logger.getLogger(Muzyka.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public void Stop() {

        c.stop();
    }

    private boolean akcteptacja(String path) {

        path = path.toLowerCase();
        if (path.endsWith(".wav")) {
            return true;
        }
        return false;
    }

    public void WylaczWlaczDzwieki() {
        if (this.wyciszony == true) {
             this.wyciszony = false;
        }
        else {
            this.wyciszony = true;
        }
       
    }
    

}
