/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pk.project.graphic.internal;

import pk.project.contracts.IGraphic;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import org.apache.felix.scr.annotations.*;
import org.osgi.framework.BundleContext;

/**
 *
 * @author Adam_2
 */
@Service
@Component//(immediate = true)
public class CutPicture implements IGraphic {

    List<Image> listOfPartsImage;
    private BundleContext context;

    public CutPicture() {

        this.listOfPartsImage = new ArrayList<Image>();
    }

    @Activate
    public void activate(BundleContext context) {
        this.context = context;
        System.out.println("Komponent graphic zaladowany.");
    }

    @Override
    public List<Image> getListOfPartsImage() {
        return this.listOfPartsImage;
    }

    @Override
    public void SetListOfPartsImage(String pathToImage, int numberParts) {
        try {
            this.listOfPartsImage = this.SplitImage(pathToImage, numberParts);
        } catch (IOException ex) {
            Logger.getLogger(CutPicture.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public List<Image> ScaleListImage(List<Image> ListImage, int maxWidth, int maxHeight) {
        List listOfScaleImage = new ArrayList<Image>();
        for (int i = 0; i < ListImage.size(); i++) {
            listOfScaleImage.add(this.ScaleImage(ListImage.get(i), maxWidth, maxHeight));
        }
        return listOfScaleImage;
    }

    //Skaluje proporcjonalnie długość i szerokość
    @Override
    public Image ScaleImage(Image image, int maxWidth, int maxHeight) {
        int scale;
        Image img = image;
        if (maxWidth < img.getWidth(null)) {
            scale = image.getWidth(null) - maxWidth;

            img = image.getScaledInstance(img.getWidth(null) - scale, img.getHeight(null) - scale, java.awt.Image.SCALE_DEFAULT);
        }
        if (maxHeight < img.getHeight(null) || (maxHeight < image.getHeight(null))) {
            scale = img.getHeight(null) - maxHeight;
            img = image.getScaledInstance(img.getWidth(null) - scale, img.getHeight(null) - scale, java.awt.Image.SCALE_DEFAULT);
        }
        return img;

    }

    private List<Image> SplitImage(String pathToImage, int numberParts) throws IOException {

        BufferedImage image = ImageIO.read(new File(pathToImage));
        Image partOfImage;
        List listOfPartsImage = new ArrayList<Image>();
        int x = 0;
        int y = 0;
        int widthPart = Math.round(image.getWidth() / numberParts);
        int heightPart = Math.round(image.getHeight() / numberParts);
        for (int i = 0; i < numberParts - 1; i++) {

            for (int j = 0; j < numberParts; j++) {
                //ostatnia część obrazka w wierszu
                if (image.getWidth() - x < widthPart + 20) {
                    partOfImage = image.getSubimage(x, y, image.getWidth() - x, heightPart);
                    listOfPartsImage.add(partOfImage);
                    x = 0;
                } else {
                    partOfImage = image.getSubimage(x, y, widthPart, heightPart);
                    listOfPartsImage.add(partOfImage);
                    x += widthPart;
                }
            }
            y += heightPart;
        }
        for (int i = 0; i < numberParts; i++) {

            if (image.getWidth() - x < widthPart + 20) {
                partOfImage = image.getSubimage(x, y, image.getWidth() - x, image.getHeight() - y);
                listOfPartsImage.add(partOfImage);
                x = 0;
            } else {
                partOfImage = image.getSubimage(x, y, widthPart, image.getHeight() - y);
                listOfPartsImage.add(partOfImage);
                x += widthPart;
            }
        }
        return listOfPartsImage;
    }

}
